<?php

date_default_timezone_set('Europe/Madrid');
$today = date("Y-m-d");

$path = '/Volumes/backup_ssd/ssh_downloads/screen_capture/';
$path_content = '/Volumes/backup_ssd/ssh_downloads/screen_capture/'.$today;

$path_aux_content = '/Users/marcroma/kec_utilities/screencapture/'.$today;

$fileName = date("H.i.s_dmY");

if (!file_exists($path_content) && file_exists($path)) {
    mkdir($path_content, 0777, true);
    $command = 'screencapture -x -t jpg '.$path_content.'/'.$fileName.'.jpg';
}else{
    mkdir($path_aux_content, 0777, true);
    $command = 'screencapture -x -t jpg '.$path_aux_content.'/'.$fileName.'.jpg';
}

shell_exec($command);
